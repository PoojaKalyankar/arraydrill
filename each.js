function each(items,cb){
    if(items.length==0){
        console.log("Array is Empty");
        return;
    }
    for(let index=0;index<items.length;index++){
        cb(items[index],index);
    }
}
module.exports=each;