function find(items,cb){
    if(items.length==0){
        return "Array is Empty";
    }
    for(let index=0;index<items.length;index++){
        if(cb(items[index])){
            return items[index];
        }
    }
}

module.exports=find;