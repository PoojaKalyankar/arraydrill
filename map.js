function map(items,cb){
    if(items.length==0){
      return "Array is Empty";
    }
    let ansArray=[];
    for(let index=0;index<items.length;index++){
      ansArray.push(cb(items,index));
    }
    return ansArray;
}

module.exports=map;