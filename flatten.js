let ansArray=[];
function flatten(nestedArray){
    if(nestedArray.length==0){
        return "nested array is empty";
    }
 for(let index=0;index<nestedArray.length;index++){
    if(Array.isArray(nestedArray[index])){
        flatten(nestedArray[index]);
    }else{
        ansArray.push(nestedArray[index]);
    }
 }
 return ansArray;
}

module.exports=flatten;