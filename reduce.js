function reduce(items,cb,startingValue){
     if(items.length==0){
        return "items is empty";
     }
    for(let index=0;index<items.length;index++){
        startingValue=cb(startingValue,items[index]);
    }
    return startingValue;
}

module.exports=reduce;